﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NBAStatesWebApp.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace NBAStatesWebApp.Controllers
{
    public class AdminController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            var model = new Tables();
            model.TableName = "League";
            return View(model);
        }

        public IActionResult EditLeague()
        {
            
            return View();
        }

        public IActionResult EditPlayerStats()
        {

            return View();
        }

        public IActionResult EditStandings()
        {

            return View();
        }

        public IActionResult EditTeams()
        {

            return View();
        }

        public IActionResult EditTeamRoster()
        {

            return View();
        }
        
        //[ValidateAntiForgeryToken]
        //public IActionResult Save(Tables model)
        //{
        //    // do something with model e.g. model.Name
        //    return ();
        //}

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Index(Tables model)
        {
            if (ModelState.IsValid)
            {
                if( model.TableName == "League")
                {
                    return RedirectToAction("EditLeague");
                }
                if (model.TableName == "Player_Stats")
                {
                    return RedirectToAction("EditPlayerStats");
                }
                if (model.TableName == "Standings")
                {
                    return RedirectToAction("EditStandings");
                }
                if (model.TableName == "Teams")
                {
                    return RedirectToAction("EditTeams");
                }
                if (model.TableName == "Team_Roster")
                {
                    return RedirectToAction("EditTeamRoster");
                }
                else
                    return RedirectToAction("Admin");

            }

            // If we got this far, something failed; redisplay form.
            return View(model);
        }
    }
}
