using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NBAStatesWebApp.Models;

namespace NBAStatesWebApp.Controllers
{
    public class PlayerStatsController : Controller
    {
        private readonly nbadatabaseContext _context;

        public PlayerStatsController(nbadatabaseContext context)
        {
            _context = context;
        }

        // GET: PlayerStats
        public async Task<IActionResult> Index()
        {
            return View(await _context.PlayerStats.ToListAsync());
        }

        // GET: PlayerStats/Details/5
        public async Task<IActionResult> Details(string TeamAbbr, byte PlayerNum)
        {
            if (!PlayerStatsExists(TeamAbbr, PlayerNum))
            {
                return NotFound();
            }
           
            var playerStats = await _context.PlayerStats
                .FirstOrDefaultAsync(m => m.TeamAbbr == TeamAbbr && m.PlayerNum == PlayerNum);

            if (playerStats == null)
            {
                return NotFound();
            }

            return View(playerStats);
        }

        // GET: PlayerStats/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: PlayerStats/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("TeamAbbr,PlayerNum,Pos,Ppg,Apg,Rpg,Fgp,Tpp,Ftp,Spg,Bpg,Gp")] PlayerStats playerStats)
        {
            if (ModelState.IsValid)
            {
                _context.Add(playerStats);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(playerStats);
        }

        // GET: PlayerStats/Edit/5
        public async Task<IActionResult> Edit(string TeamAbbr, byte PlayerNum)
        {
            if (!PlayerStatsExists(TeamAbbr, PlayerNum))
            {
                return NotFound();
            }

            var playerStats = await _context.PlayerStats.FindAsync(TeamAbbr, PlayerNum);
            if (playerStats == null)
            {
                return NotFound();
            }
            return View(playerStats);
        }

        // POST: PlayerStats/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string TeamAbbr, byte PlayerNum, [Bind("TeamAbbr,PlayerNum,Pos,Ppg,Apg,Rpg,Fgp,Tpp,Ftp,Spg,Bpg,Gp")] PlayerStats playerStats)
        {
            //DataView record = db.RecordDataView.Find(TeamAbbr, PlayerNum);
            if (TeamAbbr != playerStats.TeamAbbr && PlayerNum != playerStats.PlayerNum)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(playerStats);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PlayerStatsExists(playerStats.TeamAbbr, playerStats.PlayerNum))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(playerStats);
        }

        // GET: PlayerStats/Delete/5
        public async Task<IActionResult> Delete(string TeamAbbr, byte PlayerNum)
        {
            if (TeamAbbr == null )
            {
                return NotFound();
            }

            var playerStats = await _context.PlayerStats
                .FirstOrDefaultAsync(m => m.TeamAbbr == TeamAbbr && m.PlayerNum == PlayerNum);
            if (playerStats == null)
            {
                return NotFound();
            }

            return View(playerStats);
        }

        // POST: PlayerStats/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string TeamAbbr, byte PlayerNum)
        {
            var playerStats = await _context.PlayerStats.FindAsync(TeamAbbr, PlayerNum);
            _context.PlayerStats.Remove(playerStats);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PlayerStatsExists(string id, byte PlayerNum)
        {
            return (_context.PlayerStats.Any(e => e.TeamAbbr == id) && _context.PlayerStats.Any(e => e.PlayerNum == PlayerNum));
        }
    }
}
