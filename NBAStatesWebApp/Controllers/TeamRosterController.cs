using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NBAStatesWebApp.Models;

namespace NBAStatesWebApp.Controllers
{
    public class TeamRosterController : Controller
    {
        private readonly nbadatabaseContext _context;

        public TeamRosterController(nbadatabaseContext context)
        {
            _context = context;
        }

        // GET: TeamRoster
        public async Task<IActionResult> Index()
        {
            return View(await _context.TeamRoster.ToListAsync());
        }

        // GET: TeamRoster/Details/5
        public async Task<IActionResult> Details(string TeamAbb, byte PlayerNo)
        {
            if (!TeamRosterExists(TeamAbb, PlayerNo))
            {
                return NotFound();
            }

            var teamRoster = await _context.TeamRoster
                .FirstOrDefaultAsync(m => m.PlayerNo == PlayerNo && m.TeamAbb == TeamAbb);

            if (teamRoster == null)
            {
                return NotFound();
            }

            return View(teamRoster);
        }

        // GET: TeamRoster/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: TeamRoster/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PlayerNo,TeamAbb,Fname,Lname,Mit,Age")] TeamRoster teamRoster)
        {
            if (ModelState.IsValid)
            {
                _context.Add(teamRoster);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(teamRoster);
        }

        // GET: TeamRoster/Edit/5
        public async Task<IActionResult> Edit(string TeamAbb, byte PlayerNo)
        {
            if (!TeamRosterExists(TeamAbb, PlayerNo))
            {
                return NotFound();
            }
            //var teamRoster = await _context.TeamRoster
            //    .FirstOrDefaultAsync(m => m.TeamAbb == TeamAbb && m.PlayerNo == PlayerNo);
            //var teamRoster = await _context.TeamRoster.FindAsync(TeamAbb, PlayerNo);
            var teamRoster = await _context.TeamRoster
                .FirstOrDefaultAsync(m => m.PlayerNo == PlayerNo && m.TeamAbb == TeamAbb);
            if (teamRoster == null)
            {
                return NotFound();
            }
            return View(teamRoster);
        }

        // POST: TeamRoster/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string TeamAbb, byte PlayerNo, [Bind("PlayerNo,TeamAbb,Fname,Lname,Mit,Age")] TeamRoster teamRoster)
        {
            if (TeamAbb != teamRoster.TeamAbb && PlayerNo != teamRoster.PlayerNo)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(teamRoster);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TeamRosterExists(teamRoster.TeamAbb, teamRoster.PlayerNo))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(teamRoster);
        }

        // GET: TeamRoster/Delete/5
        public async Task<IActionResult> Delete(string TeamAbb, byte PlayerNo)
        {
            if (TeamAbb == null)
            {
                return NotFound();
            }

            var teamRoster = await _context.TeamRoster
                .FirstOrDefaultAsync(m => m.TeamAbb == TeamAbb && m.PlayerNo == PlayerNo);
            if (teamRoster == null)
            {
                return NotFound();
            }

            return View(teamRoster);
        }

        // POST: TeamRoster/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string TeamAbb, byte PlayerNo)
        {
            var teamRoster = await _context.TeamRoster.FindAsync(TeamAbb, PlayerNo);
            _context.TeamRoster.Remove(teamRoster);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TeamRosterExists(string id, byte PlayerNo)
        {
            return (_context.TeamRoster.Any(e => e.TeamAbb == id) && _context.TeamRoster.Any(e => e.PlayerNo == PlayerNo));
        }
    }
}
