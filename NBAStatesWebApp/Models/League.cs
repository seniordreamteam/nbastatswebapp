﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace NBAStatesWebApp.Models
{
    public partial class League
    {
        [Required]
        public string Conf { get; set; }

        [Required]
        public string TeamAbbr { get; set; }

        [Required]
        public string TeamName { get; set; }
    }
}
