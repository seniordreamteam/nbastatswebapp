﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace NBAStatesWebApp.Models
{
    public partial class PlayerStats
    {
        [Required]
        public string TeamAbbr { get; set; }

        [Required]
        public byte PlayerNum { get; set; }

        [Required]
        public string Pos { get; set; }

        [Required]
        public decimal? Ppg { get; set; }

        [Required]
        public decimal? Apg { get; set; }

        [Required]
        public decimal? Rpg { get; set; }

        [Required]
        public decimal? Fgp { get; set; }

        [Required]
        public decimal? Tpp { get; set; }

        [Required]
        public decimal? Ftp { get; set; }

        [Required]
        public decimal? Spg { get; set; }

        [Required]
        public decimal? Bpg { get; set; }

        [Required]
        public byte? Gp { get; set; }
    }
}
