﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace NBAStatesWebApp.Models
{
    public partial class Standings
    {
        [Required]
        public string TeamAbbr { get; set; }

        [Required]
        public string Conf { get; set; }

        public byte? Ranks { get; set; }

        [Required]
        public byte? W { get; set; }

        [Required]
        public byte? L { get; set; }


        public decimal? WPercent { get; set; }

        public byte? Gb { get; set; }
    }
}
