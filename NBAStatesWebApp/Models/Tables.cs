﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace NBAStatesWebApp.Models
{
    public class Tables
    {
        public string TableName { get; set; }

        public List<SelectListItem> TableNames { get; } = new List<SelectListItem>
        {
            new SelectListItem { Value = "League", Text = "League"  },
            new SelectListItem { Value = "Player_Stats", Text = "Player Stats"  },
            new SelectListItem { Value = "Standings", Text = "Standings"  },
            new SelectListItem { Value = "Teams", Text = "Teams" },
            new SelectListItem { Value = "Team_Roster", Text = "Team_Roster" },         
        };

    }
}
