﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NBAStatesWebApp.Models
{
    public partial class TeamRoster
    {
        [Required]
        public byte PlayerNo { get; set; }

        [Required]
        public string TeamAbb { get; set; }

        [Required]
        public string Fname { get; set; }

        [Required]
        public string Lname { get; set; }

        public string Mit { get; set; }

        [Required]
        public byte? Age { get; set; }
    }
}
