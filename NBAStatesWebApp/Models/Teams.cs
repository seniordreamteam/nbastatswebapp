﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace NBAStatesWebApp.Models
{
    public partial class Teams
    {
        [Required]
        public string TeamAbbr { get; set; }

        [Required]
        public string TeamName { get; set; }

        [Required]
        public string TeamLocation { get; set; }

        [Required]
        public string HeadCoach { get; set; }
    }
}

