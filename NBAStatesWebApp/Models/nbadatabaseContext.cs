﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace NBAStatesWebApp.Models
{
    public partial class nbadatabaseContext : DbContext
    {
        public nbadatabaseContext()
        {
        }

        public nbadatabaseContext(DbContextOptions<nbadatabaseContext> options)
            : base(options)
        {
        }

        public virtual DbSet<League> League { get; set; }
        public virtual DbSet<PlayerStats> PlayerStats { get; set; }
        public virtual DbSet<Standings> Standings { get; set; }
        public virtual DbSet<TeamRoster> TeamRoster { get; set; }
        public virtual DbSet<Teams> Teams { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=nbadatbase.database.windows.net;Database=nbadatabase;Trusted_Connection=False;MultipleActiveResultSets=true;User ID=dream;Password=myPassw0rd");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<League>(entity =>
            {
                entity.HasKey(e => e.TeamAbbr);

                entity.ToTable("LEAGUE");

                entity.Property(e => e.TeamAbbr)
                    .HasColumnName("Team_Abbr")
                    .HasMaxLength(3)
                    .ValueGeneratedNever();

                entity.Property(e => e.Conf).HasMaxLength(4);

                entity.Property(e => e.TeamName)
                    .HasColumnName("Team_Name")
                    .HasMaxLength(25);
            });

            modelBuilder.Entity<PlayerStats>(entity =>
            {
                entity.HasKey(e => new { e.TeamAbbr, e.PlayerNum });

                entity.ToTable("PLAYER_STATS");

                entity.Property(e => e.TeamAbbr).HasMaxLength(3);

                entity.Property(e => e.Apg)
                    .HasColumnName("APG")
                    .HasColumnType("decimal(7, 1)");

                entity.Property(e => e.Bpg)
                    .HasColumnName("BPG")
                    .HasColumnType("decimal(5, 1)");

                entity.Property(e => e.Fgp)
                    .HasColumnName("FGP")
                    .HasColumnType("decimal(5, 1)");

                entity.Property(e => e.Ftp)
                    .HasColumnName("FTP")
                    .HasColumnType("decimal(5, 1)");

                entity.Property(e => e.Gp).HasColumnName("GP");

                entity.Property(e => e.Pos)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Ppg)
                    .HasColumnName("PPG")
                    .HasColumnType("decimal(5, 1)");

                entity.Property(e => e.Rpg)
                    .HasColumnName("RPG")
                    .HasColumnType("decimal(2, 1)");

                entity.Property(e => e.Spg)
                    .HasColumnName("SPG")
                    .HasColumnType("decimal(5, 1)");

                entity.Property(e => e.Tpp)
                    .HasColumnName("TPP")
                    .HasColumnType("decimal(5, 1)");
            });

            modelBuilder.Entity<Standings>(entity =>
            {
                entity.HasKey(e => e.TeamAbbr);

                entity.ToTable("STANDINGS");

                entity.Property(e => e.TeamAbbr)
                    .HasColumnName("Team_Abbr")
                    .HasMaxLength(3)
                    .ValueGeneratedNever();

                entity.Property(e => e.Conf).HasMaxLength(4);

                entity.Property(e => e.Gb).HasColumnName("GB");

                entity.Property(e => e.WPercent)
                    .HasColumnName("W_Percent")
                    .HasColumnType("decimal(5, 1)");
            });

            modelBuilder.Entity<TeamRoster>(entity =>
            {
                entity.HasKey(e => new { e.PlayerNo, e.TeamAbb });

                entity.ToTable("TEAM_ROSTER");

                entity.Property(e => e.TeamAbb).HasMaxLength(3);

                entity.Property(e => e.Fname).HasMaxLength(20);

                entity.Property(e => e.Lname).HasMaxLength(20);

                entity.Property(e => e.Mit)
                    .HasColumnName("MIT")
                    .HasMaxLength(1);
            });

            modelBuilder.Entity<Teams>(entity =>
            {
                entity.HasKey(e => e.TeamAbbr);

                entity.Property(e => e.TeamAbbr)
                    .HasMaxLength(4)
                    .ValueGeneratedNever();

                entity.Property(e => e.HeadCoach)
                    .IsRequired()
                    .HasMaxLength(128);

                entity.Property(e => e.TeamLocation)
                    .IsRequired()
                    .HasMaxLength(128);

                entity.Property(e => e.TeamName)
                    .IsRequired()
                    .HasMaxLength(128);
            });
        }
    }
}
