﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Data.SqlClient;
using System.Text;


namespace NBAStatesWebApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            System.Diagnostics.Debug.WriteLine("THIS IS A TEST1");
            
            System.Diagnostics.Debug.WriteLine("THIS IS A TEST");
            try
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.DataSource = "nbadatbase.database.windows.net";
                builder.UserID = "dream";
                builder.Password = "myPassw0rd";
                builder.InitialCatalog = "nbadatabase";

                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    System.Diagnostics.Debug.WriteLine("\nQuery data example:");
                    System.Diagnostics.Debug.WriteLine("=========================================\n");

                    connection.Open();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("SELECT * ");
                    sb.Append("FROM [TEAMS] ");
                    String sql = sb.ToString();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                System.Diagnostics.Debug.WriteLine("{0} {1}", reader.GetString(0), reader.GetString(1));
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                System.Diagnostics.Debug.WriteLine(e.ToString());
            }
            //System.Diagnostics.Debug.ReadLine();

            CreateWebHostBuilder(args).Build().Run();


        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
