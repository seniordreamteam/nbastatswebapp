#pragma checksum "/Users/dianamoreno/Documents/Codebase/NBAStatesWebApp/NBAStatesWebApp/Views/PlayerStats/Delete.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "85f56bd76933e54d42e903512db4bfe9d9a50d24"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_PlayerStats_Delete), @"mvc.1.0.view", @"/Views/PlayerStats/Delete.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/PlayerStats/Delete.cshtml", typeof(AspNetCore.Views_PlayerStats_Delete))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "/Users/dianamoreno/Documents/Codebase/NBAStatesWebApp/NBAStatesWebApp/Views/_ViewImports.cshtml"
using NBAStatesWebApp;

#line default
#line hidden
#line 2 "/Users/dianamoreno/Documents/Codebase/NBAStatesWebApp/NBAStatesWebApp/Views/_ViewImports.cshtml"
using NBAStatesWebApp.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"85f56bd76933e54d42e903512db4bfe9d9a50d24", @"/Views/PlayerStats/Delete.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2c3652d498cdbeb7776363f54a334b102c59f27c", @"/Views/_ViewImports.cshtml")]
    public class Views_PlayerStats_Delete : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<NBAStatesWebApp.Models.PlayerStats>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("type", "hidden", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Index", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Delete", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(43, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 3 "/Users/dianamoreno/Documents/Codebase/NBAStatesWebApp/NBAStatesWebApp/Views/PlayerStats/Delete.cshtml"
  
    ViewData["Title"] = "Delete";

#line default
#line hidden
            BeginContext(87, 172, true);
            WriteLiteral("\r\n<h2>Delete</h2>\r\n\r\n<h3>Are you sure you want to delete this?</h3>\r\n<div>\r\n    <h4>PlayerStats</h4>\r\n    <hr />\r\n    <dl class=\"dl-horizontal\">\r\n        <dt>\r\n            ");
            EndContext();
            BeginContext(260, 44, false);
#line 15 "/Users/dianamoreno/Documents/Codebase/NBAStatesWebApp/NBAStatesWebApp/Views/PlayerStats/Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.TeamAbbr));

#line default
#line hidden
            EndContext();
            BeginContext(304, 43, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd>\r\n            ");
            EndContext();
            BeginContext(348, 40, false);
#line 18 "/Users/dianamoreno/Documents/Codebase/NBAStatesWebApp/NBAStatesWebApp/Views/PlayerStats/Delete.cshtml"
       Write(Html.DisplayFor(model => model.TeamAbbr));

#line default
#line hidden
            EndContext();
            BeginContext(388, 43, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt>\r\n            ");
            EndContext();
            BeginContext(432, 45, false);
#line 21 "/Users/dianamoreno/Documents/Codebase/NBAStatesWebApp/NBAStatesWebApp/Views/PlayerStats/Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.PlayerNum));

#line default
#line hidden
            EndContext();
            BeginContext(477, 43, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd>\r\n            ");
            EndContext();
            BeginContext(521, 41, false);
#line 24 "/Users/dianamoreno/Documents/Codebase/NBAStatesWebApp/NBAStatesWebApp/Views/PlayerStats/Delete.cshtml"
       Write(Html.DisplayFor(model => model.PlayerNum));

#line default
#line hidden
            EndContext();
            BeginContext(562, 43, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt>\r\n            ");
            EndContext();
            BeginContext(606, 39, false);
#line 27 "/Users/dianamoreno/Documents/Codebase/NBAStatesWebApp/NBAStatesWebApp/Views/PlayerStats/Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.Ppg));

#line default
#line hidden
            EndContext();
            BeginContext(645, 43, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd>\r\n            ");
            EndContext();
            BeginContext(689, 35, false);
#line 30 "/Users/dianamoreno/Documents/Codebase/NBAStatesWebApp/NBAStatesWebApp/Views/PlayerStats/Delete.cshtml"
       Write(Html.DisplayFor(model => model.Ppg));

#line default
#line hidden
            EndContext();
            BeginContext(724, 43, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt>\r\n            ");
            EndContext();
            BeginContext(768, 39, false);
#line 33 "/Users/dianamoreno/Documents/Codebase/NBAStatesWebApp/NBAStatesWebApp/Views/PlayerStats/Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.Apg));

#line default
#line hidden
            EndContext();
            BeginContext(807, 43, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd>\r\n            ");
            EndContext();
            BeginContext(851, 35, false);
#line 36 "/Users/dianamoreno/Documents/Codebase/NBAStatesWebApp/NBAStatesWebApp/Views/PlayerStats/Delete.cshtml"
       Write(Html.DisplayFor(model => model.Apg));

#line default
#line hidden
            EndContext();
            BeginContext(886, 43, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt>\r\n            ");
            EndContext();
            BeginContext(930, 39, false);
#line 39 "/Users/dianamoreno/Documents/Codebase/NBAStatesWebApp/NBAStatesWebApp/Views/PlayerStats/Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.Rpg));

#line default
#line hidden
            EndContext();
            BeginContext(969, 43, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd>\r\n            ");
            EndContext();
            BeginContext(1013, 35, false);
#line 42 "/Users/dianamoreno/Documents/Codebase/NBAStatesWebApp/NBAStatesWebApp/Views/PlayerStats/Delete.cshtml"
       Write(Html.DisplayFor(model => model.Rpg));

#line default
#line hidden
            EndContext();
            BeginContext(1048, 43, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt>\r\n            ");
            EndContext();
            BeginContext(1092, 39, false);
#line 45 "/Users/dianamoreno/Documents/Codebase/NBAStatesWebApp/NBAStatesWebApp/Views/PlayerStats/Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.Fgp));

#line default
#line hidden
            EndContext();
            BeginContext(1131, 43, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd>\r\n            ");
            EndContext();
            BeginContext(1175, 35, false);
#line 48 "/Users/dianamoreno/Documents/Codebase/NBAStatesWebApp/NBAStatesWebApp/Views/PlayerStats/Delete.cshtml"
       Write(Html.DisplayFor(model => model.Fgp));

#line default
#line hidden
            EndContext();
            BeginContext(1210, 43, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt>\r\n            ");
            EndContext();
            BeginContext(1254, 39, false);
#line 51 "/Users/dianamoreno/Documents/Codebase/NBAStatesWebApp/NBAStatesWebApp/Views/PlayerStats/Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.Tpp));

#line default
#line hidden
            EndContext();
            BeginContext(1293, 43, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd>\r\n            ");
            EndContext();
            BeginContext(1337, 35, false);
#line 54 "/Users/dianamoreno/Documents/Codebase/NBAStatesWebApp/NBAStatesWebApp/Views/PlayerStats/Delete.cshtml"
       Write(Html.DisplayFor(model => model.Tpp));

#line default
#line hidden
            EndContext();
            BeginContext(1372, 43, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt>\r\n            ");
            EndContext();
            BeginContext(1416, 39, false);
#line 57 "/Users/dianamoreno/Documents/Codebase/NBAStatesWebApp/NBAStatesWebApp/Views/PlayerStats/Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.Ftp));

#line default
#line hidden
            EndContext();
            BeginContext(1455, 43, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd>\r\n            ");
            EndContext();
            BeginContext(1499, 35, false);
#line 60 "/Users/dianamoreno/Documents/Codebase/NBAStatesWebApp/NBAStatesWebApp/Views/PlayerStats/Delete.cshtml"
       Write(Html.DisplayFor(model => model.Ftp));

#line default
#line hidden
            EndContext();
            BeginContext(1534, 43, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt>\r\n            ");
            EndContext();
            BeginContext(1578, 39, false);
#line 63 "/Users/dianamoreno/Documents/Codebase/NBAStatesWebApp/NBAStatesWebApp/Views/PlayerStats/Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.Spg));

#line default
#line hidden
            EndContext();
            BeginContext(1617, 43, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd>\r\n            ");
            EndContext();
            BeginContext(1661, 35, false);
#line 66 "/Users/dianamoreno/Documents/Codebase/NBAStatesWebApp/NBAStatesWebApp/Views/PlayerStats/Delete.cshtml"
       Write(Html.DisplayFor(model => model.Spg));

#line default
#line hidden
            EndContext();
            BeginContext(1696, 43, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt>\r\n            ");
            EndContext();
            BeginContext(1740, 39, false);
#line 69 "/Users/dianamoreno/Documents/Codebase/NBAStatesWebApp/NBAStatesWebApp/Views/PlayerStats/Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.Bpg));

#line default
#line hidden
            EndContext();
            BeginContext(1779, 43, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd>\r\n            ");
            EndContext();
            BeginContext(1823, 35, false);
#line 72 "/Users/dianamoreno/Documents/Codebase/NBAStatesWebApp/NBAStatesWebApp/Views/PlayerStats/Delete.cshtml"
       Write(Html.DisplayFor(model => model.Bpg));

#line default
#line hidden
            EndContext();
            BeginContext(1858, 43, true);
            WriteLiteral("\r\n        </dd>\r\n        <dt>\r\n            ");
            EndContext();
            BeginContext(1902, 38, false);
#line 75 "/Users/dianamoreno/Documents/Codebase/NBAStatesWebApp/NBAStatesWebApp/Views/PlayerStats/Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.Gp));

#line default
#line hidden
            EndContext();
            BeginContext(1940, 43, true);
            WriteLiteral("\r\n        </dt>\r\n        <dd>\r\n            ");
            EndContext();
            BeginContext(1984, 34, false);
#line 78 "/Users/dianamoreno/Documents/Codebase/NBAStatesWebApp/NBAStatesWebApp/Views/PlayerStats/Delete.cshtml"
       Write(Html.DisplayFor(model => model.Gp));

#line default
#line hidden
            EndContext();
            BeginContext(2018, 38, true);
            WriteLiteral("\r\n        </dd>\r\n    </dl>\r\n    \r\n    ");
            EndContext();
            BeginContext(2056, 266, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "69d3b4ffdbf041f89a47b71194b3ee00", async() => {
                BeginContext(2082, 10, true);
                WriteLiteral("\r\n        ");
                EndContext();
                BeginContext(2092, 42, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("input", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "5fb180aabf7849adaba459f027a8bbf0", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.InputTypeName = (string)__tagHelperAttribute_0.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
#line 83 "/Users/dianamoreno/Documents/Codebase/NBAStatesWebApp/NBAStatesWebApp/Views/PlayerStats/Delete.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => __model.TeamAbbr);

#line default
#line hidden
                __tagHelperExecutionContext.AddTagHelperAttribute("asp-for", __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(2134, 10, true);
                WriteLiteral("\r\n        ");
                EndContext();
                BeginContext(2144, 43, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("input", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "5b6cba33cc48452fa180d7568665f9d6", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.InputTypeName = (string)__tagHelperAttribute_0.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
#line 84 "/Users/dianamoreno/Documents/Codebase/NBAStatesWebApp/NBAStatesWebApp/Views/PlayerStats/Delete.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => __model.PlayerNum);

#line default
#line hidden
                __tagHelperExecutionContext.AddTagHelperAttribute("asp-for", __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(2187, 84, true);
                WriteLiteral("\r\n        <input type=\"submit\" value=\"Delete\" class=\"btn btn-default\" /> |\r\n        ");
                EndContext();
                BeginContext(2271, 38, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "245108c489f94c9797b286dce658d7da", async() => {
                    BeginContext(2293, 12, true);
                    WriteLiteral("Back to List");
                    EndContext();
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_1.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(2309, 6, true);
                WriteLiteral("\r\n    ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Action = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(2322, 10, true);
            WriteLiteral("\r\n</div>\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<NBAStatesWebApp.Models.PlayerStats> Html { get; private set; }
    }
}
#pragma warning restore 1591
